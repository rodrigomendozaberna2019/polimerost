sendQuotation = () => {
    let form = document.getElementById("quotation-form");
    let formData = new FormData(form);
    $.ajax({
        url: '/quotation',
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            $('#dialog-message').text("Hemos recibido tu cotización correctamente, nos pondremos en contacto contigo lo antes posible.");
            $('#quotation-form').trigger("reset");
            $('#successModal').modal('show');
        }
    }).fail( function( jqXHR ) {
        switch(jqXHR.status) {
            case 400:
                let errors = "";
                $.each(jqXHR.responseJSON, function (key, info) {
                    $.each(info, function (x, data) {
                        errors += data + "<br />";
                    });
                });
                $('#dialog-failure-message').append(errors);
                $('#failureModal').modal('show');
                break;

            case 500:
                $('#dialog-failure-message').text("Hubo un erro al enviar tu mensaje, vuelve a intentarlo.");
                $('#failureModal').modal('show');
                break;
        }
    });
};

sendContactMessage= () => {
    let form = document.getElementById("contact-form");
    let formData = new FormData(form);
    $.ajax({
        url: '/contact',
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            $('#dialog-message').text("Hemos recibido tu mensaje correctamente, nos pondremos en contacto contigo lo antes posible.");
            $('#contact-form').trigger("reset");
            $('#successModal').modal('show');
        }
    }).fail( function( jqXHR ) {
        switch(jqXHR.status) {
            case 400:
                let errors = "";
                $.each(jqXHR.responseJSON, function (key, info) {
                    $.each(info, function (x, data) {
                        errors += data + "<br />";
                    });
                });
                $('#dialog-failure-message').append(errors);
                $('#failureModal').modal('show');
                break;

            case 500:
                $('#dialog-failure-message').text("Hubo un erro al enviar tu mensaje, vuelve a intentarlo.");
                $('#failureModal').modal('show');
                break;
        }
    });
};