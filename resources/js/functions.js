//Preview files
$(function () {
    $("#FileUpload").on("change", function () {
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // Check if File is selected, or no FileReader support
        var ReaderObj = new FileReader(); // Create instance of the FileReader
        ReaderObj.readAsDataURL(files[0]); // read the file uploaded
        ReaderObj.onloadend = function () { // set uploaded image data as background of div
            $("#PreviewPicture").css("background-image", "url(" + this.result + ")");
        }
    });
});

//Scroll Top
$("#scroll-down").click(function () {
    $('html, body').animate({
        scrollTop: $("#home-carucel").offset().top - 70
    }, 1000);
});

//Scroll option header
$("#home-materias-header").click(function () {
    $('html, body').animate({
        scrollTop: $("#home-materias").offset().top - 70
    }, 1000);
});

//Scroll option header
$("#home-caroucel-header").click(function () {
    $('html, body').animate({
        scrollTop: $("#home-carucel").offset().top - 70
    }, 1000);
});

//Scroll option header
$("#home-servicios-header").click(function () {
    $('html, body').animate({
        scrollTop: $("#home-servicios").offset().top - 70
    }, 1000);
});
