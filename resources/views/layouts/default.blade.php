<!doctype html>
<html>
<head>
@include('components.head')
<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('/js/app.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
@include('components.whatsappButton')
@include('components.failureDialog')
@include('components.successDialog')
@include('components.header')
@yield('content')
@include('components.footer')

<!-- Scrips -->
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
