<!-- Modal feedback -->
<div class="modal" id="successModal" tabindex="-1" role="dialog" aria-labelledby="successModal"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content success-modal">
            <div class="modal-body text-center">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <img src="{{ asset('svg/logotipo.svg') }}" width="180" height="80">
                            <button type="button" class="close success-close" data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="row py-4 justify-content-center">
                        <div class="col">
                            <img src="{{ asset('svg/success.svg') }}" width="250" height="120">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col col-lg-7 col-sm-12" id="dialog-message">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
