<!-- Header -->
<nav class="navbar nav nav-cover navbar-expand-lg fixed-top py-3">
    <div class="container">
        <!-- Brand -->
        <a class="navbar-brand nav-height" href="/">
            <img src="{{ asset('svg/logotipo.svg') }}" width="180" height="80">
        </a>
        <!-- Mobile toggler -->
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarText"
                aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars text-muted"></i>
        </button>
        <!-- Top menu -->
        <div class="collapse navbar-collapse justify-content-end nav-padding" id="navbarText">
            <ul class="navbar-nav ml-auto align-items-center">
                <li class="nav-item">
                    <a id="home-caroucel-header" class="nav-link {{ request()->path() == '#' ? 'nav-active' : 'false' }}" href="/#home-carucel">¿Qué es el
                        retomoldeo?</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->path() == 'about' ? 'nav-active' : 'false' }}"
                       href="/about">Nosotros</a>
                </li>
                <li class="nav-item">
                    <a id="home-materias-header" class="nav-link {{ request()->path() == '#' ? 'nav-active' : 'false' }}" href="/#home-materias">Materias
                        Primas</a>
                </li>
                <li class="nav-item">
                    <a id="home-servicios-header" class="nav-link {{ request()->path() == '#' ? 'nav-active' : 'false' }}"
                       href="/#home-servicios">Servicios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->path() == 'quotation' ? 'nav-active' : 'false' }}"
                       href="/quotation">Cotiza tu proyecto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->path() == 'contact' ? 'nav-active' : 'false' }}"
                       href="/contact">Contáctanos</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<script>
    $(window).scroll(function () {
        $('nav').toggleClass('scrolled', $(this).scrollTop() > 50);
    });
</script>
