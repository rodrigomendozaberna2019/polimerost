<!-- Footer -->
<div class="container py-4">
    <div class="row justify-content-start">
        <div class="col col-lg-3 col-sm-6 small-title">
            <img src="{{ asset('svg/logotipo.svg') }}" width="180" height="80">
            <p>Copyright © All rights reserved.</p>
        </div>
        <div class="col col-lg-1 hidemobile footer-secction">
            <div class="footer-divider"></div>
        </div>
        <div class="col col-lg-3 col-sm-6 footer-menu small-title">
            <ul>
                <li><a class="footer-link" href="/privacy">Aviso de privacidad</a></li>
                <li><a class="footer-link" href="/about">Nosotros</a></li>
                <li><a class="footer-link" href="/contact">Contáctanos</a></li>
            </ul>
        </div>
    </div>
</div>
