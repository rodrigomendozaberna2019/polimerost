<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Polímeros Transnacionales') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div class="wrapper">
    <table class="header">
        <tr class="profile-logo">
            <td> <h1>Nuevo mensaje de contacto</h1> </td>
        </tr>
        <tr class="text-content">
            <td> Se recibió una nuevo mensaje de contacto</td>
        </tr>
        <tr class="text-content">
            <td>Nombre: </td>
            <td>{{$name}}</td>
        </tr>
        <tr class="text-content">
            <td>Email: </td>
            <td>{{$email}}</td>
        </tr>
        <tr class="text-content">
            <td>Asunto: </td>
            <td>{{$affair}}</td>
        </tr>
        <tr class="text-content">
            <td>Descripción: </td>
            <td>{{$description}}</td>
        </tr>
    </table>
</div>
</body>

<style>
    @import url("https://fonts.googleapis.com/css?family=Raleway:300,400,600");
    @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro');

    *{
        font-family: "Source Sans Pro", Helvetica, Arial, sans-serif;
    }

    @page{
        size: letter;
        margin: 0;
        padding: 0;
    }
    body{
        display: block;
        text-align: -webkit-center;
        margin: 0;
        padding: 5px;
    }

    .wrapper{
        width: 100vw;
    }

    table{
        width: 95%;
        padding-left: 2.5%;
        padding-right: 2.5%;
    }

    .header{
        background-color: #0A93E8;
        color: #fff;
        padding-top: 30px;
        padding-bottom: 40px;
    }

    .profile-logo{
        text-align: center;
    }

    .border-y-blue{
        border-top: 1px solid #0A93E8;
        border-bottom: 1px solid #0A93E8;
    }

    .text-content>td{
        padding-top: 15px;
        padding-bottom: 15px;
        text-align: center;
    }

    .footer-disclaimer{
        font-size: 12px;
    }

    .btn{
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        border: 1px solid transparent;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        color: #6c757d;
        background-color: transparent;
        background-image: none;
        border-color: #6c757d;
    }

    a{
        text-decoration: none;
    }
</style>
</html>
