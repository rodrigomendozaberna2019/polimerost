@extends('layouts.default')
@section('content')
    <!-- Contact page-->
    <div class="contact">
        <div class="container contact-container">
            <h1 class="font-weight-bold text-uppercase">Contáctanos</h1>
            <dl class="row">
                <dd class="col col-sm-9 col-xl-4">
                    <p>
                        Gracias por interesarte en nuestra compañía, te responderemos a la brevedad con la
                        información necesaria.
                    </p>
                </dd>
            </dl>
            <dl class="justify-content-center">
                <div class="col col-lg-4 col-sm-12">
                    <ul>
                        <li class="py-3">
                            <img src="{{ asset('svg/contact/wp.svg') }}" class="mr-4" width="40px" height="auto">
                            +52 (442) 439 03 69
                        </li>
                        <li class="py-3">
                            <img src="{{ asset('svg/contact/email.svg') }}" class="mr-4" width="40px" height="auto">
                            ventas@polimerost.com
                        </li>
                        <li class="py-3">
                            <img src="{{ asset('svg/contact/ubication.svg') }}" class="mr-4" width="40px" height="auto">
                            El Marqués, Querétaro.
                        </li>
                    </ul>
                </div>
                <div class="col col-lg-8 col-sm-12">
                    <p>
                        Tu solicitud es muy importante para nosotros, trata de ser lo más específico.
                    </p>
                    <dd class="col-sm-12 col-xl-6 p-0">
                        <form id="contact-form">
                            <div class="form-group">
                                <input type="text" class="form-control quotation-input" id="name" name="name"
                                       placeholder="Nombre completo">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control quotation-input"
                                       id="email" name="email"
                                       placeholder="Correo electrónico">
                            </div>
                            <div class="form-group">
                                <select class="custom-select quotation-input" id="affair" name="affair">
                                    <option selected>Asunto</option>
                                    <option>Materia prima negro</option>
                                    <option>Materia prima virgen</option>
                                    <option>Pulverizado de polietileno de media densidad (MDPE)</option>
                                    <option>Peletizado de polímeros</option>
                                    <option>Maquila de productos</option>
                                    <option>Diseño de producto</option>
                                </select>
                            </div>
                            <div class="form-group">
                                    <textarea class="form-control quotation-input" id="description" name="description"
                                              rows="5"
                                              placeholder="Descripción"></textarea>
                            </div>
                        </form>
                    </dd>
                </div>
            </dl>
            <div class="justify-content-center">
                <!-- BUTTON SEND-->
                <div class="row col-12 justify-content-center">
                    <p>
                        <a href="#" class="btn btn-primary my-2 quotation-button"
                           onclick="sendContactMessage()">Enviar</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@stop
