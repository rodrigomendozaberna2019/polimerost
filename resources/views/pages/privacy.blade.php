@extends('layouts.default')
@section('content')
    <!-- Privacy page -->
    <div class="container contact-container">
        <h1 class="font-weight-bold text-uppercase">Aviso de privacidad</h1>
        <dl class="row privacy-row">
            <dd class="col-md-5">
                <h3 class="privacy-subtitle">Datos personales que solicitamos</h3>
                <p>La información solicitada al usuario en la sección de contacto es:</p>
                <ul>
                    <li>Nombre</li>
                    <li>Teléfono</li>
                    <li>Dirección de correo electrónico</li>
                </ul>
                <p>
                    Polímeros transnacionales utilizará estos datos recabados para hacerle llegar información sobre
                    nuestros servicios. Sus datos personales no serán compartidos con terceros ni serán transferidos ni
                    tratados dentro o fuera del país, por personas distintas a esta empresa.*
                </p>
                <p>
                    *Excepto en casos especiales cuando pensemos que proporcionar esta información puede servir para
                    identificar, localizar o realizar acciones legales contra personas que pudiesen infringir las
                    condiciones del servicio del sitio propiedad de polímeros transnacionales, siempre y cuando hayan
                    sido requeridos por orden judicial para cumplir con las disposiciones procesales correspondientes.
                </p>
                <h3 class="privacy-subtitle">Qué son los cookies y cómo se utilizan</h3>
                <p>
                    Los cookies son pequeñas piezas de información que son enviadas por el sitio web a su navegador y se
                    almacenan en el disco duro de su equipo y se utilizan para determinar sus preferencias cuando se
                    conecta a los servicios de nuestro sitio, así como para rastrear determinados comportamientos o
                    actividades llevadas a cabo por usted dentro de nuestro sitio.
                </p>
                <p>
                    En algunas secciones de nuestro sitio requerimos que el cliente tenga habilitados los cookies ya que
                    algunas de las funciones requieren de éstas para trabajar. La utilización de cookies no será
                    utilizada para identificar a los usuarios, con excepción de los casos en que se investiguen posibles
                    actividades fraudulentas.
                </p>
                <p>
                    Si el usuario quisiera deshabilitar las cookies, deberá hacerlo directamente en su explorador de
                    internet.
                </p>
                <h3 class="privacy-subtitle">Trasferencias de información con terceros</h3>
                <p>
                    Polímeros transnacionales no realiza transferencias de información con terceros.
                </p>

            </dd>
            <dd class="col-md-5 offset-md-2">
                <h3 class="privacy-subtitle">Datos personales sensibles</h3>
                <p>
                    Polímeros transnacionales no solicita datos personales sensibles en el sitio.
                </p>
                <h3 class="privacy-subtitle">Cambios en el aviso de privacidad</h3>
                <p>
                    Este aviso puede tener cambios en el futuro, de ser así se le avisará oportunamente en la sección de
                    Aviso de Privacidad del sitio.
                </p>
                <h3 class="privacy-subtitle">Confidencialidad de los menores</h3>
                <p>
                    Polímeros transnacionales no solicita información de identificación personal a los menores. Los
                    menores siempre deben solicitar permiso a sus padres o tutores antes de enviar información personal
                    a otro usuario que se encuentre en línea o ingresarla en cualquier sitio web. Si identificamos que
                    algún menor de edad ha ingresado información, ésta se eliminará automáticamente.
                </p>
                <h3 class="privacy-subtitle">Aceptación de los términos</h3>
                <p>
                    Esta declaración de Confidencialidad / Privacidad constituye un acuerdo legal entre el usuario y
                    polímeros transnacionales.
                </p>
                <p>
                    Si el usuario utiliza los servicios de este sitio web, significa que ha leído, entendido y
                    consentido los términos antes expuestos.
                </p>
                <p>
                    Para resolver cualquier duda en este sentido y cualquiera referente a este aviso de privacidad, nos
                    podrá contactar.
                </p>
                <h3 class="privacy-subtitle">Autoridad</h3>
                <p>
                    Si el usuario considera que han sido vulnerados sus derechos respecto de la protección de datos
                    personales, tiene el derecho de acudir a la autoridad correspondiente para defender su ejercicio. La
                    autoridad es el Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos
                    Personales (INAI), su sitio web es: www.inai.org.mx
                </p>
            </dd>
        </dl>
        <!-- Button contact -->
        <div class="row">
            <div class="container text-center">
                <h2 class="font-weight-bold text-uppercase">¿Tienes alguna duda?</h2>
                <p>
                    <a href="/contact" class="btn btn-primary my-2 about-button font-weight-bold">Contáctanos</a>
                </p>
            </div>
        </div>
    </div>
@stop
