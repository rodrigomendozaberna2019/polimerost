@extends('layouts.default')
@section('content')
    <!-- Quotation page -->
    <div class="quotation">
        <div class="container quotation-container">
            <h1 class="font-weight-bold text-uppercase">Tu cotización</h1>
            <dl class="row">
                <dd class="col-sm-9 col-xl-4">
                    <p>
                        Gracias por interesarte en nuestra compañía, te responderemos a la brevedad con la
                        información necesaria.
                    </p>
                </dd>
            </dl>
            <dl class="row justify-content-center">
                <div class="col col-lg-4 col-sm-12 hidemobile">

                </div>
                <div class="col col-lg-8 col-sm-12">
                    <p>
                        Tu solicitud es muy importante para nosotros, trata de ser lo más específico.
                    </p>
                    <dd class="col-sm-12 col-xl-7 p-0">
                        <form id="quotation-form">
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                    <input type="text" class="form-control quotation-input" id="name" name="name"
                                           placeholder="Nombre completo">
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text" class="form-control quotation-input" id="phone" name="phone"
                                           placeholder="Teléfono">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-8">
                                    <div class="row row-cols-1">
                                        <div class="form-group col">
                                            <input type="email" class="form-control quotation-input"
                                                   id="email"
                                                   name="email"
                                                   placeholder="Correo electrónico">
                                        </div>
                                        <div class="form-group col">
                                            <select class="custom-select quotation-input" id="affair" name="affair">
                                                <option selected>Asunto</option>
                                                <option>Materia prima negro</option>
                                                <option>Materia prima virgen</option>
                                                <option>Pulverizado de polietileno de media densidad (MDPE)</option>
                                                <option>Peletizado de polímeros</option>
                                                <option>Maquila de productos</option>
                                                <option>Diseño de producto</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 text-center">
                                    <label id="PreviewPicture" for="FileUpload" class="quotation-input">
                                        <i class="fas fa-paperclip fa-2x" style="padding: 1rem 0"></i>
                                        <br/>
                                        Adjuntar archivos (PDF, JPG, PNG)
                                        <input type="file" class="form-control-file quotation-file" id="FileUpload"
                                               name="FileUpload"
                                               accept="image/jpeg, image/png, application/pdf">
                                    </label>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                            <textarea class="form-control quotation-input" id="description" name="description" rows="5"
                                      placeholder="Descripción"></textarea>
                                </div>
                            </div>
                        </form>
                    </dd>
                </div>
            </dl>
            <div class="row">
                <div class="container text-center">
                    <p>
                        <a href="#" class="btn btn-primary my-2 quotation-button" onclick="sendQuotation()">Enviar</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@stop
