@extends('layouts.default')
@section('content')
    <div class="about">
        <div class="container about-container">
            <h1 class="font-weight-bold text-uppercase">¿Quiénes somos?</h1>
        </div>
        <div class="about-image">
            <h5 class="about-subtitle py-4">Empresa de rotomoldeo orgullosamente mexicana</h5>
            <p class="about-description">Somos una empresa encargada de hacer tus proyectos realidad,
                somos una empresa de pulverizado de materias primas de la mejor calidad, con otros múltiples
                servicios, tenemos la mejor maquinaria y la mano de obra más capaz para que tu materia prima o tu
                proyecto tenga los mejores estándares de calidad. </p>
        </div>
        <div class="row py-5">
            <div class="container about-items">
                <div class="col-lg-4">
                    <img class="rounded-circle" src="{{ asset('svg/mision.svg') }}" alt="Mision" width="auto"
                         height="45">
                    <div class="w-100 w-space"></div>
                    <h2 class="large-title py-3">MISIÓN</h2>
                    <div class="w-100 w-space"></div>
                    <p class="col-lg-10 p-0">Brindar una amplia gama de productos de rotomoldeo que se
                        adecuen a las necesidades de nuestros clientes, siempre teniendo en alto nuestra calidad y
                        servicio. así como apoyar en el desarrollo de nuevos proyectos en el mundo del rotomoldeo.</p>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <img class="rounded-circle" src="{{ asset('svg/vision.svg') }}" alt="Vision" width="auto"
                         height="40">
                    <div class="w-100 w-space"></div>
                    <h2 class="large-title py-3">VISIÓN</h2>
                    <div class="w-100 w-space"></div>
                    <p class="col-lg-10 p-0">Desarrollar el mundo del rotomoldeo en México, por medio de
                        nuestros productos y servicios.</p>
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4">
                    <img class="rounded-circle" src="{{ asset('svg/valores.svg') }}" alt="Valores" width="auto"
                         height="45">
                    <div class="w-100 w-space"></div>
                    <h2 class="large-title py-3">VALORES</h2>
                    <div class="w-100 w-space"></div>
                    <ul class="col-lg-10 p-0">
                        <li class="py-2"><strong>1.- Honestidad:</strong> Brindando siempre la calidad y el servicio que
                            nos
                            distingue.
                        </li>
                        <li class="py2"><strong>2.- Trabajo en equipo:</strong> Trabajar en conjunto con nuestros
                            clientes apoyando en sus proyectos y necesidades.
                        </li>
                        <li class="py-2"><strong>3.- Aprendizaje:</strong> Siempre estando en constante actualización.
                        </li>
                        <li class="py-2"><strong>4.- Integridad:</strong> Siempre teniendo rectitud en nuestros acuerdos
                            y compromisos.
                        </li>
                        <li class="py-2"><strong>5.- Servicio:</strong> Siempre teniendo disponibilidad hacia nuestros
                            clientes.
                        </li>
                    </ul>
                </div><!-- /.col-lg-4 -->
            </div>
        </div>
        <!-- BUTTON ABOUT CONTACT -->
        <div class="row">
            <div class="container text-center">
                <p>
                    <a href="/contact" class="btn btn-primary my-2 about-button">Contáctanos</a>
                </p>
            </div>
        </div>
    </div>
@stop
