@extends('layouts.default')
@section('content')
    <!-- HOME BANNER -->
    <div class="banner">
        <div class="container h-100 text-center text-lg-right banner-container">
            <div class="row justify-content-lg-end justify-content-sm-center">
                <div class="col  col-sm-12 col-xl-5">
                    <h1 class="font-weight-bold text-uppercase">Los mejores en materia prima para rotomoldeo</h1>
                </div>
            </div>
            <div class="row justify-content-lg-end justify-content-sm-center">
                <div class="col col-sm-12 col-xl-3">
                    <p>
                        El proceso de rotomoldeo es grandioso y muy versátil, la mejor materia prima y la mejor
                        maquinaria.
                    </p>
                </div>
            </div>
            <div class="row justify-content-lg-end justify-content-sm-center">
                <div class="col py-5">
                    <p>
                        <a href="/quotation" class="btn btn-primary my-2 banner-button">Cotiza tu proyecto</a>
                    </p>
                </div>
            </div>
            <div class="row justify-content-lg-end justify-content-sm-center">
                <div class="col">
                    <button type="button" id="scroll-down">
                        <img src="{{ asset('svg/scroll.svg') }}" width="25px" height="auto">
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- HOME CARUCEL -->
    <div class="py-5" id="home-carucel">
        <div class="container">
            <h1 class="font-weight-bold text-uppercase">¿Qué es el retomoldeo?</h1>
            <!-- Desktop -->
            <div id="carouselExampleSlidesOnly" class="carousel slide carousel-fade hidemobile" data-ride="carousel"
                 style="height: 25rem;">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">

                    <div class="carousel-item active">
                        <div class="card-group">
                            <div class="card mb-3 home-carucel-card-small" style="max-width: 240px;">
                                <a href="#carouselExampleSlidesOnly" role="button"
                                   data-slide="prev">
                                    <div class="row no-gutters" role="button" data-slide="prev">
                                        <div class="col-md-4 home-carucel-small-title">
                                            <h2 class="large-title">Uso</h2>
                                        </div>
                                        <div class="col-md-8 home-carucel-small-img home-carucel-bg-uso">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="card mb-3 home-carucel-card-large">
                                <div class="row no-gutters">
                                    <div class="col-md-8 home-carucel-large-img home-carucel-bg-sectores">
                                    </div>
                                    <div class="col-md-4 home-carucel-large-title">
                                        <h2 class="large-title text-center">Sectores</h2>
                                        <p>Los productos pueden ser para los sectores:</p>
                                        <ul style="padding: 0 1rem">
                                            <li>- Médico</li>
                                            <li>- Agrónomo</li>
                                            <li>- Arquitectura</li>
                                            <li>- Interiorismo</li>
                                            <li>- Partes de transporte</li>
                                            <li>- Equipo deportivo</li>
                                            <li>- Mobiliario</li>
                                            <li>- Exteriores</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-3 home-carucel-card-small" style="max-width: 240px;">
                                <a href="#carouselExampleSlidesOnly" role="button"
                                   data-slide="next">
                                    <div class="row no-gutters">
                                        <div class="col-md-8 home-carucel-small-img home-carucel-bg-ejemplos">
                                        </div>
                                        <div class="col-md-4 home-carucel-small-title">
                                            <h2 class="large-title">Ejemplos</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="card-group">
                            <div class="card mb-3 home-carucel-card-small" style="max-width: 240px;">
                                <a href="#carouselExampleSlidesOnly" role="button"
                                   data-slide="prev">
                                    <div class="row no-gutters">
                                        <div class="col-md-4 home-carucel-small-title">
                                            <h2 class="large-title">Sectores</h2>
                                        </div>
                                        <div class="col-md-8 home-carucel-small-img home-carucel-bg-sectores">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="card mb-3 home-carucel-card-large">
                                <div class="row no-gutters">
                                    <div class="col-md-8 home-carucel-large-img home-carucel-bg-ejemplos">
                                    </div>
                                    <div class="col-md-4 home-carucel-large-title">
                                        <h2 class="large-title text-center">Ejemplos</h2>
                                        <ul style="padding: 0 1rem">
                                            <li>- Tinacos</li>
                                            <li>- Kayaks</li>
                                            <li>- Macetas</li>
                                            <li>- Sillas</li>
                                            <li>- Camastros</li>
                                            <li>- Botes de basura</li>
                                            <li>- Señalización vial</li>
                                            <li>- Cajas</li>
                                            <li>- Entre muchos otros</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-3 home-carucel-card-small" style="max-width: 240px;">
                                <a href="#carouselExampleSlidesOnly" role="button"
                                   data-slide="next">
                                    <div class="row no-gutters">
                                        <div class="col-md-8 home-carucel-small-img home-carucel-bg-uso">
                                        </div>
                                        <div class="col-md-4 home-carucel-small-title">
                                            <h2 class="large-title">Uso</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <div class="card-group">
                            <div class="card mb-3 home-carucel-card-small" style="max-width: 240px;">
                                <a href="#carouselExampleSlidesOnly" role="button"
                                   data-slide="prev">
                                    <div class="row no-gutters">
                                        <div class="col-md-4 home-carucel-small-title">
                                            <h2 class="large-title">Ejemplos</h2>
                                        </div>
                                        <div class="col-md-8 home-carucel-small-img home-carucel-bg-ejemplos">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="card mb-3 home-carucel-card-large">
                                <div class="row no-gutters">
                                    <div class="col-md-8 home-carucel-large-img home-carucel-bg-uso">
                                    </div>
                                    <div class="col-md-4 home-carucel-large-title">
                                        <h2 class="large-title text-center">Uso</h2>
                                        <p>El rotomoldeo es un método utilizado para producir
                                            piezas huecas de plástico.</p>
                                        <p>Se asocia a la manufactura de diferentes piezas
                                            grandes y medianas de plástico.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-3 home-carucel-card-small" style="max-width: 240px;">
                                <a href="#carouselExampleSlidesOnly" role="button"
                                   data-slide="next">
                                    <div class="row no-gutters">
                                        <div class="col-md-8 home-carucel-small-img home-carucel-bg-sectores">
                                        </div>
                                        <div class="col-md-4 home-carucel-small-title">
                                            <h2 class="large-title">Sectores</h2>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile -->
            <ul class="hidedesktop">
                <li class="list-group-item slider-card">
                    <div class="card mb-3 slider-item">
                        <img src="{{ asset('images/ejemplos.jpg') }}" class="card-img" alt="...">
                        <div class="card-body">
                            <h2 class="font-weight-bold text-uppercase">Ejemplos</h2>
                            <ul class="small-title" style="padding: 0 3rem">
                                <li>- Tinacos</li>
                                <li>- Kayaks</li>
                                <li>- Macetas</li>
                                <li>- Sillas</li>
                                <li>- Camastros</li>
                                <li>- Botes de basura</li>
                                <li>- Señalización vial</li>
                                <li>- Cajas</li>
                                <li>- Entre muchos otros</li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="list-group-item slider-card">
                    <div class="card mb-3 slider-item">
                        <img src="{{ asset('images/sectores.jpg') }}" class="card-img" alt="...">
                        <div class="card-body">
                            <h2 class="font-weight-bold text-uppercase">Sectores</h2>
                            <ul class="small-title" style="padding: 0 3rem">
                                <li>- Médico</li>
                                <li>- Agrónomo</li>
                                <li>- Arquitectura</li>
                                <li>- Interiorismo</li>
                                <li>- Partes de transporte</li>
                                <li>- Equipo deportivo</li>
                                <li>- Mobiliario</li>
                                <li>- Exteriores</li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="list-group-item slider-card">
                    <div class="card mb-3 slider-item">
                        <img src="{{ asset('images/uso.jpg') }}" class="card-img" alt="...">
                        <div class="card-body">
                            <h2 class="font-weight-bold text-uppercase">Uso</h2>
                            <p class="small-title">El rotomoldeo es un método utilizado para producir
                                piezas huecas de plástico.</p>
                            <p class="small-title">Se asocia a la manufactura de diferentes piezas
                                grandes y medianas de plástico.</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <!-- HOME PROCESO -->
    <div class="py-5" id="home-proceso">
        <div class="container">
            <h1 class="font-weight-bold text-uppercase">Proceso de retomoldeo</h1>
            <!-- Desktop -->
            <div class="hidemobile">
                <img src="{{ asset('images/proceso.jpg') }}" width="100%" height="auto" alt="proceso">
            </div>
            <!-- Mobile -->
            <div class="hidedesktop text-center">
                <img src="{{ asset('images/procesomobile.png') }}" width="80%" height="auto" alt="procesomobile">
            </div>
        </div>
    </div>

    <!-- HOME MATERIAS PRIMAS -->
    <div class="py-5" id="home-materias">
        <div class="subjects-bg">
            <div class="container text-lg-right text-center">
                <h1 class="large-title  subjects-space-1">Nuestras <br/> materias primas</h1>
                <p>Somos expertos en producir <br/> <strong>materia prima</strong> para rotomoldeo.</p>
                <strong class="subjects-space-2">Polietileno de media dencidad (MDPE)</strong>
                <ul class="py-5">
                    <li><strong>- Natural</strong></li>
                    <li><strong>- Negro</strong></li>
                </ul>
                <p>
                    La <strong>calidad</strong> de la <strong>materia prima</strong> <br/> siempre será indispensable
                    <br/> para un producto de primera.
                </p>
                <p class="subjects-space-1">
                    <a href="/quotation" class="btn btn-primary subjects-button">
                        <img src="{{ asset('svg/quotation.svg') }}" class="mr-2" width="20px" height="auto">
                        Solicitar cotización
                    </a>
                </p>
            </div>
        </div>
        <!-- Desktop -->
        <div class="container subjects-card hidemobile">
            <div class="row justify-content-start">
                <div class="col-4 subjects-figures">
                    <p class="pt-4"><strong>Polietileno de <br> media <br> densidad</strong> (MDPE)</p>
                    <h2 class="large-title col-lg-12 p-0">NATURAL</h2>
                </div>
                <div class="col-4 offset-md-1 subjects-figures">
                    <p class="pt-4"><strong>Polietileno de <br> media <br> densidad</strong> (MDPE)</p>
                    <h2 class="large-title">NEGRO</h2>
                </div>
            </div>
            <div class="row justify-content-start">
                <div class="col-1">
                    <p class="pt-3">Larga duración de vida</p>
                </div>
                <div class="col-3 subjects-list">
                    <dl class="row mt-3">
                        <dt class="col-sm-2 m-0 subjects-num">1.</dt>
                        <dd class="col-sm-10 pt-3 m-0">Índice de fusión
                            <p class="m-0 subjects-medida">5.0 g/10 min</p>
                        </dd>
                        <dt class="col-sm-2 m-0 subjects-num">2.</dt>
                        <dd class="col-sm-10 pt-3 m-0">Densidad
                            <p class="m-0 subjects-medida">0.935 g/cm3</p>
                        </dd>
                        <dt class="col-sm-2 m-0 subjects-num">3.</dt>
                        <dd class="col-sm-10 pt-3 m-0">Fuerza de flexibilidad
                            <p class="m-0 subjects-medida">590 MPa</p>
                        </dd>
                        <dt class="col-sm-2 m-0 subjects-num">4.</dt>
                        <dd class="col-sm-10 pt-3 m-0">Temperatura de desviación bajo carga
                            <p class="m-0 subjects-medida">61 ºC</p>
                        </dd>
                    </dl>
                </div>
                <div class="col-1 offset-md-1">
                    <p class="pt-3">Más intenso y más brillante</p>
                </div>
                <div class="col-3 subjects-list">
                    <dl class="row mt-3">
                        <dt class="col-sm-2 m-0 subjects-num">1.</dt>
                        <dd class="col-sm-10 pt-3 m-0">Índice de fluidez
                            <p class="m-0 subjects-medida">5.0 g/10 min</p>
                        </dd>
                        <dt class="col-sm-2 m-0 subjects-num">2.</dt>
                        <dd class="col-sm-10 pt-3 m-0">Densidad (astm-d1505)
                            <p class="m-0 subjects-medida">0.935 g/cm3</p>
                        </dd>
                        <dt class="col-sm-2 m-0 subjects-num">3.</dt>
                        <dd class="col-sm-10 pt-3 m-0">Flujo seco (astm-d1895-6)
                            <p class="m-0 subjects-medida">590 MPa</p>
                        </dd>
                        <dt class="col-sm-2 m-0 subjects-num">4.</dt>
                        <dd class="col-sm-10 pt-3 m-0">Protección UV</dd>
                        <dt class="col-sm-2 m-0 subjects-num">5.</dt>
                        <dd class="col-sm-10 pt-3 m-0">Resistencia al impacto</dd>
                    </dl>
                </div>
                <div class="col text-center subjects-help">
                    <div class="col-12">
                        <p class="font-weight-bold">¿Tienes alguna duda?</p>
                        <p>
                            <a href="/contact" class="btn btn-primary subjects-btn-contact">Contáctanos</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile -->
        <div class="container hidedesktop">
            <div class="list-group subjects-border">
                <a class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h1 class="font-weight-bold">NATURAL</h1>
                    </div>
                    <p class="mb-1">Polietileno de media densidad (mdpe)</p>
                    <ul>
                        <li><span class="subjects-num">1.-</span> Índice de fusión</li>
                        <li><span class="subjects-num">2.-</span> Densidad</li>
                        <li><span class="subjects-num">3.-</span> Fuerza de flexibilidad</li>
                        <li><span class="subjects-num">4.-</span> Temperatura de desviación bajo carga</li>
                    </ul>
                </a>
                <a class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h1 class="font-weight-bold">NEGRO</h1>
                    </div>
                    <p class="mb-1">Polietileno de media densidad (mdpe)</p>
                    <ul>
                        <li><span class="subjects-num">1.-</span> Índice de fluidez</li>
                        <li><span class="subjects-num">2.-</span> Densidad (astm-d1505)</li>
                        <li><span class="subjects-num">3.-</span> Flujo seco (astm-d1895-6)</li>
                        <li><span class="subjects-num">4.-</span> Protección UV</li>
                        <li><span class="subjects-num">5.-</span> Resistencia al impacto</li>
                    </ul>
                </a>
            </div>
            <div class="row pt-4 justify-content-center text-center">
                <div class="col-12">
                    <p>¿Tienes alguna duda?</p>
                </div>
                <div class="col-12">
                    <a href="/contact" class="btn btn-primary services-btn-contact">Contáctanos</a>
                </div>
            </div>
        </div>
    </div>

    <!-- HOME NUESTROS SERVICIOS -->
    <div class="py-5" id="home-servicios">
        <div class="container">
            <h1 class="font-weight-bold text-uppercase">Nuestros servicios</h1>
        </div>
        <div>
            <img src="{{ asset('images/services.jpg') }}" width="100%" height="auto">
        </div>
        <!-- Desktop -->
        <div class="container hidemobile">
            <div class="row row-cols-2 services-colum">
                <div class="col services-lines d-flex align-items-end">
                    <span class="services-num large-title" style="font-size: 2.5rem;">1.</span>
                    <span style="font-size: 1.5rem; margin: 1rem">Pulverizado de polietileno de media densidad</span>
                </div>
            </div>
            <div class="row row-cols-2" style="margin: -5px">
                <div class="col services-buttons d-flex align-items-end" style="padding: 0">
                    <p>
                        <a href="/quotation" class="btn btn-primary services-btn-quotation">
                            <img src="{{ asset('svg/quotation.svg') }}" class="mr-2" width="20px" height="auto">
                            Solicitar cotización
                        </a>
                        <a href="/contact" class="btn btn-primary services-btn-contact">Contáctanos</a>
                    </p>
                </div>
                <div class="col" style="padding: 0;margin-left: -10px;display: flex;">
                    <div class="services-items">
                        <h4 class="services-num font-weight-bold">2.</h4>
                        <p>Peletizado de polímeros</p>
                    </div>
                    <div class="services-items" style="margin: 0 1rem;">
                        <h4 class="services-num font-weight-bold">3.</h4>
                        <p>Maquila de productos de rotomoldeo, máximo horno de 2.5 metros</p>
                    </div>
                    <div class="services-items">
                        <h4 class="services-num font-weight-bold">4.</h4>
                        <p>Diseño de producto</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile -->
        <div class="container hidedesktop">
            <div class="list-group services-border">
                <a class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h1 class="font-weight-bold services-num">1.</h1>
                    </div>
                    <p class="mb-1">Pulverizado de polietileno de media densidad (mdpe)</p>
                </a>
                <a class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h1 class="font-weight-bold services-num">2.</h1>
                    </div>
                    <p class="mb-1">Peletizado de polímeros</p>
                </a>
                <a class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h1 class="font-weight-bold services-num">3.</h1>
                    </div>
                    <p class="mb-1">Maquila de productos de rotomoldeo, máximo horno de 2.5 metros</p>
                </a>
                <a class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h1 class="font-weight-bold services-num">4.</h1>
                    </div>
                    <p class="mb-1">Diseño de producto </p>
                </a>
            </div>
            <div class="row pt-4 justify-content-center">
                <p>
                    <a href="/quotation" class="btn btn-primary services-btn-quotation">
                        <img src="{{ asset('svg/quotation.svg') }}" class="mr-2" width="20px" height="auto">
                        Solicitar cotización
                    </a>
                </p>
                <p>
                    <a href="/contact" class="btn btn-primary services-btn-contact">Contáctanos</a>
                </p>
            </div>
        </div>
    </div>

    <!-- HOME TRABAJAR CON NOSOTROS -->
    <div class="py-5">
        <div class="container">
            <h1 class="font-weight-bold text-uppercase">¿Por qué trabajar con nosotros?</h1>
            <div class="working-bg"></div>
            <div class="card-deck">
                <div class="card working-card">
                    <div class="card-body">
                        <p class="card-text">Localización estratégica <br/> El Marqués, Querétaro</p>
                        <img src="{{ asset('svg/location.svg') }}" width="100px" height="auto">
                    </div>
                </div>
                <div class="card working-card">
                    <div class="card-body">
                        <p class="card-text">Maquinaria de alta <br/> tecnología</p>
                        <img src="{{ asset('svg/tech.svg') }}" width="100px" height="auto">
                    </div>
                </div>
                <div class="card working-card">
                    <div class="card-body">
                        <p class="card-text">Materias primas de <br/> primera calidad</p>
                        <img src="{{ asset('svg/primera.svg') }}" width="100px" height="auto">
                    </div>
                </div>
                <div class="card working-card">
                    <div class="card-body">
                        <p class="card-text">Mano de obra <br/> calificada</p>
                        <img src="{{ asset('svg/obra.svg') }}" width="100px" height="auto">
                    </div>
                </div>
            </div>
            <div class="row py-5 justify-content-center">
                <p class="p-2">
                    <a href="/contact" class="btn btn-primary working-btn-contact">Contáctanos</a>
                </p>
                <p class="p-2">
                    <a href="/about" class="btn btn-primary working-btn-about">Conócenos</a>
                </p>
            </div>
        </div>
    </div>

@endsection
