<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Mail;

class MailController extends Controller
{
    public function sendQuotation(Request $request)
    {
        try {
            $messages = [
                'name.required'             => 'El nombre es obligatorio.',
                'phone.required'            => 'El teléfono es obligatorio.',
                'email.required'            => 'El correo electrónico es obligatorio.',
                'description.required'      => 'La descripción es obligatoria.',
                'affair.required'           => 'El asunto es obligatorio.',
                'FileUpload.required'       => 'El archivo de referencia es obligatorio.',
            ];

            $validator = Validator::make($request->all(), [
                'name'              => 'required',
                'phone'             => 'required',
                'email'             => 'required',
                'description'       => 'required',
                'affair'            => 'required',
                'FileUpload'        => 'required',
            ], $messages);

            if ($validator->fails()) {
                return response()->json([
                    'errors' => $validator->errors()
                ], 400);
            }

            $data = array(
                'name'              => $request->name,
                'phone'             => $request->phone,
                'email'             => $request->email,
                'description'       => $request->description,
                'affair'            => $request->affair,
            );

            Mail::send('mails.quotation', $data, function ($message) use ($request) {
                //remitente
                $message->from($request->email, env('MAIL_FROM_NAME', 'Info Polímeros Transnacionales'));

                //asunto
                $message->subject("Nueva cotización");

                //receptor
                $message->to('ventas@polimerost.com');


                //cotización
                $message->attach($request->FileUpload->getRealPath(), array(
                        'as' => 'attached file.' . $request->FileUpload->getClientOriginalExtension(),
                        'mime' => $request->FileUpload->getMimeType())
                );
            });

            return response()->json(array('msg' => "Mail enviado con éxito"), 200);

        } catch (\ErrorException $e) {
            Log::info($e);
            return response()->json(array('msg' => "Error al enviar tu cotización"), 500);
        }
    }

    public function sendContactMessage(Request $request)
    {
        try {
            $messages = [
                'name.required'             => 'El nombre es obligatorio.',
                'email.required'            => 'El correo electrónico es obligatorio.',
                'description.required'      => 'La descripción es obligatoria.',
                'affair.required'           => 'El asunto es obligatorio.',
            ];

            $validator = Validator::make($request->all(), [
                'name'              => 'required',
                'email'             => 'required',
                'description'       => 'required',
                'affair'            => 'required',
            ], $messages);

            if ($validator->fails()) {
                return response()->json([
                    'errors' => $validator->errors()
                ], 400);
            }

            $data = array(
                'name'              => $request->name,
                'email'             => $request->email,
                'description'       => $request->description,
                'affair'            => $request->affair,
            );

            Mail::send('mails.contact', $data, function ($message) use ($request) {
                //remitente
                $message->from($request->email, env('MAIL_FROM_NAME', 'Info Polímeros Transnacionales'));

                //asunto
                $message->subject("Mensaje de contacto");

                //receptor
                $message->to('ventas@polimerost.com');
            });

            return response()->json(array('msg' => "Mail enviado con éxito"), 200);

        } catch (\ErrorException $e) {
            Log::info($e);
            return response()->json(array('msg' => "Error al enviar tu mensaje"), 500);
        }
    }
}
