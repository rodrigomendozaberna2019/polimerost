<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return View::make('pages.home');
});
Route::get('about', function () {
    return View::make('pages.about');
});
Route::get('contact', function () {
    return View::make('pages.contact');
});
Route::get('quotation', function () {
    return View::make('pages.quotation');
});
Route::get('privacy', function () {
    return View::make('pages.privacy');
});
Route::post('/quotation', 'MailController@sendQuotation');
Route::post('/contact', 'MailController@sendContactMessage');
